package com.company;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Main {

    public static void main(String[] args) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();

        int today = gregorianCalendar.get(Calendar.DAY_OF_MONTH);
        int month = gregorianCalendar.get(Calendar.MONTH);

        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);

        int weekDay = gregorianCalendar.get(Calendar.DAY_OF_WEEK);
        int firstDayOfWeek = gregorianCalendar.getFirstDayOfWeek();
        int indend = 0;

        while (weekDay != firstDayOfWeek) {
            indend++;
            gregorianCalendar.add(Calendar.DAY_OF_MONTH,-1);
            weekDay = gregorianCalendar.get(Calendar.DAY_OF_WEEK);
        }


        String[] weedDaysNames = new DateFormatSymbols().getShortWeekdays();

        do {
            System.out.printf("%4s", weedDaysNames[weekDay]);
            gregorianCalendar.add(Calendar.DAY_OF_MONTH,1);
            weekDay = gregorianCalendar.get(Calendar.DAY_OF_WEEK);
        } while (weekDay != firstDayOfWeek);
        System.out.println();

        for(int i = 1; i<= indend; i++) {
            System.out.print("    ");
        }

        gregorianCalendar.set(Calendar.DAY_OF_MONTH, 1);

        do {
            int day = gregorianCalendar.get(Calendar.DAY_OF_MONTH);
            System.out.printf("%3d", day);
            if (day == today) {
                System.out.print("*");
            }else {
                System.out.print(" ");
            }
            gregorianCalendar.add(Calendar.DAY_OF_MONTH, 1);
            weekDay = gregorianCalendar.get(Calendar.DAY_OF_WEEK);

            if(weekDay == firstDayOfWeek) {
                System.out.println();
            }
        } while (gregorianCalendar.get(Calendar.MONTH) == month);

        if(weekDay != firstDayOfWeek) {
            System.out.println();
        }



    }
}
